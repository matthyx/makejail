# needed by typo3 installs in /usr/typo3/bin
chroot="/miniroot"
recursiveCopy=["/usr/lib/ImageMagick-6.3.7", "/usr/share/ImageMagick-6.3.7"]
testCommandsInsideJail=["/usr/typo3/bin/convert -version"]
processNames=["convert"]
